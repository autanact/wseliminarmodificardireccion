
/**
 * WsEliminarModificarDireccionServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsEliminarModificarDireccionServiceSkeleton java skeleton for the axisService
     */
    public class WsEliminarModificarDireccionServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsEliminarModificarDireccionServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoDireccionExistente
                                     * @param codigoDireccionNuevo
                                     * @param tipoOperacion
         */
        

                 public co.net.une.www.gis.GisRespuestaGeneralType eliminarModificarDireccion
                  (
                  java.lang.String codigoDireccionExistente,java.lang.String codigoDireccionNuevo,java.lang.String tipoOperacion
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoDireccionExistente",codigoDireccionExistente);params.put("codigoDireccionNuevo",codigoDireccionNuevo);params.put("tipoOperacion",tipoOperacion);
		try{
		
			return (co.net.une.www.gis.GisRespuestaGeneralType)
			this.makeStructuredRequest(serviceName, "eliminarModificarDireccion", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    